﻿using UnityEngine;
using System.Collections;

public class DestroyObject : MonoBehaviour {

	public float destroyTime = 0.3f;

	void Start () {
		Invoke("DestroyMyObject", destroyTime); 
	}

	void DestroyMyObject() {
		Destroy(gameObject);
	}
}