﻿using UnityEngine;
using System.Collections;

public class CollisionDamageBullet : MonoBehaviour {

	public int health = 1;
	public int damage = 1;
	public GameObject collisionAnimation;
	public bool isTwoGuns = false;

	void OnTriggerEnter2D() {
		Instantiate(collisionAnimation, transform.position, Quaternion.identity);
		health--;
	}
	
	void Update() {
		if (health <= 0) {
			DestroyMyObject();
		}
	}
	
	void DestroyMyObject() {
		Destroy(gameObject);
	}
}