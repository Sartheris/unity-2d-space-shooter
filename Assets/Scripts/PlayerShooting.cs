﻿using UnityEngine;
using System.Collections;

public class PlayerShooting : MonoBehaviour {

    public Vector3 bulletOffset = new Vector3(0, 0.5f, 0);
    public float fireDelay = 0.25f;
    GameObject leftGun, rightGun;

    public GameObject bulletPrefab;

    public GameObject superWeaponCharge;
    public GameObject superWeaponReady;

    public AudioClip clip1;

    float cooldownTimer = 0;
    Vector3 offset;

    float timeNeeded;
    float timeNeededRasenshuriken;
	int rasenshurikenRotationSpeed;
    float chargeScale;
    float maxScale;
    float chargeTime;
    
    bool rasenganCharged;
    bool rasenganCreated;
    bool rasenshurikenCreated;
    bool rasenshurikenCharged;
    bool rasenshurikenFired;

    GameObject goRasengan;
    GameObject goRasenshuriken;
    SpriteRenderer spriteRend;

    /** MEASURE ONE SECOND TEMPLATE
    float tickPeriod = 1.0f; <= The time that needs to have ticks on
    void MeasureOneSecond() {
        if (tickPeriod <= Time.deltaTime) {
            Debug.Log("1 second has passed");
            tickPeriod = 1.0f; <= Reset the timer to original tick period required
        } else {
            tickPeriod -= Time.deltaTime;
        }
    }**/

    void Start() {
        leftGun = GameObject.Find("LeftGun");
        rightGun = GameObject.Find("RightGun");
        ResetSuperWeapon();
    }

    void Update() {
        cooldownTimer -= Time.deltaTime;
        offset = transform.rotation * bulletOffset * 2;
        if (Input.GetButton("Fire1") && cooldownTimer <= 0) {
            Fire();
        }
        CheckForSuperweapon();
    }

    void CheckForSuperweapon() {
        // RASENGAN CHARGE
        if (Input.GetButton("Fire2")) {
            PositionObjects();
            if (!rasenganCreated) {
                goRasengan = (GameObject) Instantiate(superWeaponCharge, transform.position + offset, transform.rotation);
                rasenganCreated = true;
            }
            if (timeNeeded <= chargeTime && !rasenganCharged) {
                timeNeeded = 1.0f;
                ChargeRasengan();
            } else if (timeNeeded > chargeTime && !rasenganCharged) {
                timeNeeded -= Time.deltaTime;
            } else if (rasenganCharged) {
                // RASEN SHURIKEN CHARGE
                if (!rasenshurikenCreated) {
                    goRasenshuriken = (GameObject) Instantiate(superWeaponReady, transform.position + offset, transform.rotation);
                    spriteRend = goRasenshuriken.GetComponentInChildren<SpriteRenderer>();
                    rasenshurikenCreated = true;
                    if (goRasengan != null) {
                        Destroy(goRasengan);
                    }
                }
                if (spriteRend != null) {
                    spriteRend.transform.Rotate(Vector3.back * rasenshurikenRotationSpeed * Time.deltaTime * 1000);
                }
                if (!rasenshurikenCharged) {
                    if (timeNeeded <= Time.deltaTime) {
                        timeNeeded = 0.5f;
                        ChargeRasenshuriken();
                    } else {
                        timeNeeded -= Time.deltaTime;
                    }
                }
                if (rasenshurikenCharged && goRasenshuriken != null) {
                    // FIRE FORWARD
                    if (!rasenshurikenFired) {
                        AudioSource[] audio = goRasenshuriken.GetComponents<AudioSource>();
                        audio[1].PlayOneShot(clip1);
                    }
                    Vector3 pos = goRasenshuriken.transform.position;
                    Vector3 velocity = new Vector3(0, 50f * Time.deltaTime, 0);
                    pos += goRasenshuriken.transform.rotation * velocity;
                    goRasenshuriken.transform.position = pos;
                    rasenshurikenFired = true;
                }
            }
        }
        // CANCEL SUPER WEAPON
        if (Input.GetButtonUp("Fire2")) {
            CancelSuperWeapon();
            ResetSuperWeapon();
        }
    }

    void PositionObjects() {
        if (goRasengan != null) {
            goRasengan.transform.position = transform.position + offset;
        }
        if (goRasenshuriken != null && !rasenshurikenFired) {
            goRasenshuriken.transform.position = transform.position + offset;
            goRasenshuriken.transform.rotation = transform.rotation;
        }
    }

    void ChargeRasengan() {
        goRasengan.transform.localScale += new Vector3(chargeScale, chargeScale, chargeScale);
        chargeScale += 0.001f;
        if (chargeScale >= maxScale) {
            rasenganCharged = true;
        }
    }

    void ChargeRasenshuriken() {
        rasenshurikenRotationSpeed += 1;
        if (rasenshurikenRotationSpeed >= 7) {
            rasenshurikenCharged = true;
        }
    }

    void CancelSuperWeapon() {
        if (goRasengan != null) {
            // reverse it
            Destroy(goRasengan);
        }
        if (goRasenshuriken != null) {
            Destroy(goRasenshuriken);
            // recreate reverse rasengan
            // destroy it
        }
    }

    void ResetSuperWeapon() {
        timeNeeded = 1.0f;
        chargeScale = 0.0f;
        maxScale = 0.02f;
        chargeTime = 0.95f;
        rasenganCharged = false;
        rasenganCreated = false;
        rasenshurikenRotationSpeed = 3;
        rasenshurikenCreated = false;
        rasenshurikenCharged = false;
        rasenshurikenFired = false;
    }

    void Fire() {
		if (bulletPrefab.GetComponent<CollisionDamageBullet>().isTwoGuns) {
			InstantiateBullets(true, 0.0f);
		} else {
			InstantiateBullets(false, 0.1f);
		}
    }

	void InstantiateBullets(bool twoWeapons, float rateOfFire) {
		cooldownTimer = fireDelay + rateOfFire;
		Vector3 offset = transform.rotation * bulletOffset;
		if (twoWeapons) {
			Instantiate(bulletPrefab, leftGun.transform.position + offset, transform.rotation);
			Instantiate(bulletPrefab, rightGun.transform.position + offset, transform.rotation);
		} else {
			Instantiate(bulletPrefab, transform.position + offset, transform.rotation);
		}
	}
}