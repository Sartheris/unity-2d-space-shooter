﻿using UnityEngine;
using System.Collections;

public class EnemySpawner : MonoBehaviour {

	public GameObject enemy1;
	public GameObject enemy2;
	public GameObject enemy3;
	public GameObject enemy4;
    public GameObject enemy5;
    public GameObject boss;

	public float spawnDistance = 12f;
	public float enemySpawnCooldown = 5;
	public float nextEnemy = 1;
    public int enemiesUntilBoss = 32;

	Vector3 offset;
    PlayerSpawner spawner;
    bool bossSpawned = false;

    void Start() {
        GameObject go = GameObject.Find("RespawnPoint");
        spawner = go.GetComponent<PlayerSpawner>();
    }
	
	void Update () {
        if (!bossSpawned && spawner.score < enemiesUntilBoss) {
            // spawn normal enemies
            nextEnemy -= Time.deltaTime;
            if (nextEnemy <= 0) {
				nextEnemy = enemySpawnCooldown;
				enemySpawnCooldown *= 0.9f;
				if (enemySpawnCooldown < 2) {
					enemySpawnCooldown = 2;
                }
                offset = Random.onUnitSphere;
                offset.z = 0;
                offset = offset.normalized * spawnDistance;
                SpawnRandomEnemy();
            }
        } else {
            // spawn boss
            if (!bossSpawned) {
                Instantiate(boss, transform.position + offset, Quaternion.identity);
                bossSpawned = true;
            }
        }
	}

	void SpawnRandomEnemy() {
		int i = Random.Range(1, 6);
		switch (i) {
		case 1:
			Instantiate(enemy1, transform.position + offset, Quaternion.identity);
			break;
		case 2:
			Instantiate(enemy2, transform.position + offset, Quaternion.identity);
			break;
		case 3:
			Instantiate(enemy3, transform.position + offset, Quaternion.identity);
			break;
		case 4:
			Instantiate(enemy4, transform.position + offset, Quaternion.identity);
			break;
        case 5:
            Instantiate(enemy5, transform.position + offset, Quaternion.identity);
            break;
		default:
			break;
		}
	}
}