﻿using UnityEngine;
using System.Collections;

public class EnemyCollision : MonoBehaviour {

	public int health = 3;
	public GameObject deathAnimation;

    PlayerSpawner spawner;

    void Start() {
        GameObject go = GameObject.Find("RespawnPoint");
        spawner = go.GetComponent<PlayerSpawner>();
    }

	void OnTriggerEnter2D(Collider2D other) {
		if (other.GetComponent<CollisionDamageBullet>()) {
			CollisionDamageBullet bullet = other.GetComponent<CollisionDamageBullet>();
			health -= bullet.damage;
		}
	}

	void Update() {
		if (health <= 0) {
			Die();
		}
	}
	
	void Die() {
        spawner.score++;
		Instantiate(deathAnimation, transform.position, Quaternion.identity);
		Destroy(gameObject);
	}
}