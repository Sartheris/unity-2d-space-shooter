﻿using UnityEngine;
using System.Collections;

public class EnemyShooting : MonoBehaviour {

	public Vector3 bulletOffset = new Vector3(0, 0.5f, 0);
	public GameObject bulletPrefab;
	public float fireDelay = 0.5f;
	public int distanceToPlayer = 4;

	float cooldownTimer = 0;
	Transform player;

	void Update () {
		if (player == null) {
			GameObject go = GameObject.FindWithTag("Player");
			if (go != null) {
				player = go.transform;
			}
		}
		cooldownTimer -= Time.deltaTime;
		if (cooldownTimer <= 0 && player != null && Vector3.Distance(transform.position, player.position) < distanceToPlayer) {
			cooldownTimer = fireDelay;
			Vector3 offset = transform.rotation * bulletOffset;
			Instantiate(bulletPrefab, transform.position + offset, transform.rotation);
		}
	}
}