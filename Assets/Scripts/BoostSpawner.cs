﻿using UnityEngine;
using System.Collections;

public class BoostSpawner : MonoBehaviour {

	public GameObject boost1;
	public GameObject boost2;
	public GameObject boost3;
	public GameObject boost4;

	Vector3 gameArea;
	int boostsGiven = 0;
	bool givingBoosts = true;
	float boostSpawnPeriod;

	void Start () {
		// get screen boundaries
		float screenRatio = (float) Screen.width / (float) Screen.height;
		float widthOrthographic = Camera.main.orthographicSize * screenRatio;
		gameArea.y = Camera.main.orthographicSize;
		gameArea.x = widthOrthographic;
		// random boost time
		boostSpawnPeriod = Random.Range(10f, 20f);
	}
	
	void Update () {
		if (givingBoosts) {
			if (boostSpawnPeriod <= Time.deltaTime) {
				// spawn boost
				float coordsX = Random.Range(0, gameArea.x);
				float coordsY = Random.Range(0, gameArea.y);
				Vector3 boostCoords = new Vector3(coordsX, coordsY);
				SpawnRandomBoost(boostCoords);
				Debug.Log("SPAWNED BOOST");
				boostsGiven++;
				if (boostsGiven >= 3) {
					givingBoosts = false;
				} else {
					boostSpawnPeriod = Random.Range(10f, 30f);
				}
			} else {
				boostSpawnPeriod -= Time.deltaTime;
			}
		}
	}

	void SpawnRandomBoost(Vector3 boostCoords) {
		int i = Random.Range(1, 101);
		if (i > 0 && i < 40 ) {
			Instantiate(boost1, boostCoords, Quaternion.identity);
		} else if (i >= 40 && i < 70) {
			Instantiate(boost2, boostCoords, Quaternion.identity);
		} else if (i >= 70 && i < 90) {
			Instantiate(boost3, boostCoords, Quaternion.identity);
		} else if (i >= 90 && i < 101) {
			Instantiate(boost4, boostCoords, Quaternion.identity);
		}
	}
}