﻿using UnityEngine;
using System.Collections;

public class PlayerSpawner : MonoBehaviour {

	public GameObject playerPrefab;
	public int numberOfLives = 3;
	public int score = 0;

	int health;
	GameObject playerInstance;
	float respawnTimer;
	float invulnTimer = 0;
	const int initialLayer = 8;
	SpriteRenderer sr;

	void Start () {
		if (playerPrefab.GetComponent<SpriteRenderer>() != null) {
			sr = playerPrefab.GetComponent<SpriteRenderer>();
		} else {
			sr = playerPrefab.GetComponentInChildren<SpriteRenderer>();
		}
		SpawnPlayer();
	}
	
	void Update () {
		if (playerInstance == null && numberOfLives > 0) {
			respawnTimer -= Time.deltaTime;
			if (respawnTimer <= 0) {
				SpawnPlayer();
			}
		}
		if (playerInstance != null) {
			health = playerInstance.GetComponent<PlayerCollision>().health;
		}
	}

	void SpawnPlayer() {
		numberOfLives--;
		respawnTimer = 1;
		playerInstance = (GameObject) Instantiate(playerPrefab, transform.position, Quaternion.identity);
		ApplyInvulnerability(sr);
	}

	void ApplyInvulnerability(SpriteRenderer sr) {
		//Debug.Log("APPLY Invulnerability");
		if (invulnTimer > 0) {
			invulnTimer -= Time.deltaTime;
			if (invulnTimer <= 0) {
				//Debug.Log("Invulnerability");
				playerInstance.layer = initialLayer;
				if (sr != null) {
					sr.enabled = true;
				}
			} else {
				//Debug.Log("Invulnerability else");
				if (sr != null) {
					sr.enabled = !sr.enabled;
				}
			}
		}
	}

	void OnGUI() {
		GUI.Box(new Rect(10, 10, 100, 100), "");
		if (GUI.Button(new Rect(5, 70, 100, 30), new GUIContent ("Restart Game", "This is the tooltip"))) {
			Application.LoadLevel("asd");
		}
		if (GUI.Button(new Rect(5, 100, 100, 30), "Exit")) {
			Application.Quit();
		}
		// This part display any tooltip the mouse hovers over
		GUI.Label(new Rect(10, 130, 100, 50), GUI.tooltip);

		GUI.Label(new Rect(10, 10, 100, 50), "Score: " + score);
		GUI.Label(new Rect(10, 25, 100, 50), "Health: " + health);
		GUI.Label(new Rect(10, 40, 100, 50), "Lives Left: " + numberOfLives);
		if (numberOfLives <= 0 && playerInstance == null) {
			GUI.Label(new Rect(Screen.width/2 - 50, Screen.height/2 - 25, 100, 50), "Game Over");
		}
	}

	void WindowFunction(int windowID) {
		// Draw any Controls inside the window here
	}
}