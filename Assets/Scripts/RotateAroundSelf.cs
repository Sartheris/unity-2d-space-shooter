﻿using UnityEngine;
using System.Collections;

public class RotateAroundSelf : MonoBehaviour {

	public int rotationSpeed = 2;

	SpriteRenderer spriteRend;

	void Start () {
		spriteRend = GetComponent<SpriteRenderer>();
	}
	
	void Update () {
		spriteRend.transform.Rotate(Vector3.back * rotationSpeed * Time.deltaTime * 1000);
	}
}