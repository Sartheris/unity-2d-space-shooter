﻿using UnityEngine;
using System.Collections;

public class BoostObject : MonoBehaviour {

	public float tickPeriod = 0.001f;
	public float growTimes = 0.0f;
	public float growScale = 0.01f;

	public GameObject bulletPrefab;

	float growth = 1.0f;
	
	void Update () {
		// scale up and down
		if (tickPeriod <= Time.deltaTime) {
			if (growTimes >= 10) {
				growTimes = 0.0f;
				growth = -growth;
			}
			transform.localScale += new Vector3(growScale * growth, growScale * growth, growScale * growth);
			tickPeriod = 0.1f;
			growTimes++;
		} else {
			tickPeriod -= Time.deltaTime;
		}
	}
}