﻿using UnityEngine;
using System.Collections;

public class Moving : MonoBehaviour {

    public float maxSpeed = 3.5f;
    public float rotationSpeed = 180f;

    ParticleSystem particles;
    Vector3 pos;
    float shipBoundaryRadius = 0.5f;

    void Start() {
        particles = gameObject.GetComponentInChildren<ParticleSystem>();
    }

    void Update() {
        particles.Emit(1);


        Quaternion rot = transform.rotation;
        float z = rot.eulerAngles.z;
        z -= Input.GetAxis("Horizontal") * rotationSpeed * Time.deltaTime;
        rot = Quaternion.Euler(0, 0, z);
        transform.rotation = rot;
        pos = transform.position;
        Vector3 velocity = new Vector3(0, Input.GetAxis("Vertical") * maxSpeed * Time.deltaTime, 0);
        pos += rot * velocity;
        CheckScreenBoundaries();
        transform.position = pos;
    }

    void CheckScreenBoundaries() {
        if (pos.y + shipBoundaryRadius > Camera.main.orthographicSize) {
            pos.y = Camera.main.orthographicSize - shipBoundaryRadius;
        }
        if (pos.y - shipBoundaryRadius < -Camera.main.orthographicSize) {
            pos.y = -Camera.main.orthographicSize + shipBoundaryRadius;
        }
        float screenRatio = (float) Screen.width / (float) Screen.height;
        float widthOrthographic = Camera.main.orthographicSize * screenRatio;
        if (pos.x + shipBoundaryRadius > widthOrthographic) {
            pos.x = widthOrthographic - shipBoundaryRadius;
        }
        if (pos.x - shipBoundaryRadius < -widthOrthographic) {
            pos.x = -widthOrthographic + shipBoundaryRadius;
        }
    }
}