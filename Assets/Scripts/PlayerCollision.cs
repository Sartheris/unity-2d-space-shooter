﻿using UnityEngine;
using System.Collections;

public class PlayerCollision : MonoBehaviour {

	public int health = 3;
	public GameObject deathAnimation;

	int initialLayer;
	float invulnTimer = 0;
	SpriteRenderer spriteRend;

	void Start() {
		initialLayer = gameObject.layer;
		spriteRend = GetComponent<SpriteRenderer>();
		if (spriteRend == null) {
			spriteRend = transform.GetComponentInChildren<SpriteRenderer>();
			if (spriteRend == null) {
				Debug.Log("Error: " + gameObject.name + " has no sprite renderer.");
			}
		}
	}

	void OnTriggerEnter2D(Collider2D other) {
		if (other.GetComponent<CollisionDamageBullet>()) {
			// Bullet collision
			CollisionDamageBullet bullet = other.GetComponent<CollisionDamageBullet>();
			health -= bullet.damage;
			invulnTimer = 1.0f;
			gameObject.layer = 10;
		} else if (other.tag == "Boost") {
			Destroy(other.gameObject);
			gameObject.GetComponent<PlayerShooting>().bulletPrefab = other.GetComponent<BoostObject>().bulletPrefab;
		} else {
			// Other collision
			health--;
			invulnTimer = 1.0f;
			gameObject.layer = 10;
		}
	}

	void Update() {
		if (health <= 0) {
			Die();
		} else {
			ApplyInvulnerability();
		}
	}

	void ApplyInvulnerability() {
		if (invulnTimer > 0) {
			invulnTimer -= Time.deltaTime;
			if (invulnTimer <= 0) {
				Debug.Log("REMOVE Invulnerability");
				gameObject.layer = initialLayer;
				if (spriteRend != null) {
					spriteRend.enabled = true;
				}
			} else {
				if (spriteRend != null) {
					spriteRend.enabled = !spriteRend.enabled;
				}
			}
		}
	}

	void Die() {
		Instantiate(deathAnimation, transform.position, Quaternion.identity);
		Destroy(gameObject);
	}
}